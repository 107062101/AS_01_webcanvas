// Defining variables
var first = 1,
    canvas = document.getElementById("canvas"), 
    ctx = canvas.getContext("2d"),
    isMouseDown = false, 
    allInputs = document.querySelectorAll("input[type='text']");
    cStep = -1;

// Canvas size
canvas.width = 800;
canvas.height = 500;

// Events in canvas
canvas.addEventListener("mousedown", drawTrue, false);
canvas.addEventListener("click", function(){return false;}, false);
canvas.addEventListener("mouseup", drawFalse, false);
canvas.addEventListener("mouseout", drawFalse, false);
canvas.addEventListener("mousemove", draw, false);

// Change input value
// If key up or key right >> input value +1
// If key down or key left >> input value -1
for(i = 0; i < allInputs.length; i++) {
  allInputs[i].addEventListener("keydown", function(ev){
    ev = ev || window.event;
    // Check if value is number
      if(!isNaN(this.value)){
      // Read keyCode from event and when it's arrow up or right, value will increment
      if(ev.keyCode == "38" || ev.keyCode == "39"){
        ++this.value;
      }
      // If event keyCode will be from arrow down or left, value will decrement
      else if(ev.keyCode == "40" || ev.keyCode == "37"){
        // Check if value isn't zero. If is value stay same.
        if(this.value !== 0){
          --this.value;
        }
      }
    }
    // If value isn't number
    else {
      return false;
    }
  }, false);
}

// Declaring more event listeners
document.getElementById("button_clear").addEventListener("click", clear, false);
document.getElementById("size_range").addEventListener("change", sizeSelectRange, false);
document.getElementById("size_text").addEventListener("keyup", sizeSelectText, false);
document.getElementById("bristles_range").addEventListener("change", bristlesSelectRange, false);
document.getElementById("bristles_text").addEventListener("keyup", bristlesSelectText, false);


// Function that select brush size
function sizeSelectRange() {
  document.getElementById("size_text").value = document.getElementById("size_range").value;
  Brush.size = document.getElementById("size_text").value;
}

// Function that select brush size if it's changed by text input
function sizeSelectText() {
  document.getElementById("size_range").value = parseInt(document.getElementById("size_text").value, 10);
  Brush.size = document.getElementById("size_text").value;
}

// Function that select bristles value
function bristlesSelectRange() {
  document.getElementById("bristles_text").value = document.getElementById("bristles_range").value;
  Brush.number = document.getElementById("bristles_text").value;
}

// Function that select bristles value from text input
function bristlesSelectText() {
  document.getElementById("bristles_range").value = parseInt(document.getElementById("bristles_text").value, 10);
  Brush.number = document.getElementById("bristles_text").value;
}

// This function return selected color
function colorSelect() {
  colors = document.getElementsByName("colors");
  for(i = 0; i < colors.length; i++) {
    colors[i].addEventListener("click", function(){
      Brush.color = this.style.background;
      document.getElementById("type_of_option").style.background = this.style.background;
    }, false);
  }
}

// Defning object Brush
Brush = {
  size : 10,
  number : 10,
  color : colorSelect()
};

// Defining object BrushType
BrushType = {
  // Function that select brush
  selectBrushBefore: function() {
    if(document.getElementById("normal").checked === true){
      return BrushType.normalBefore();
    }
    else if(document.getElementById("weird").checked === true){
      return BrushType.weirdBefore();
    }
    else if(document.getElementById("earser").checked === true){
      return BrushType.earserBefore();
    }
    },
  selectBrushAfter: function() {
    if(document.getElementById("normal").checked === true){
      return BrushType.normalAfter();
    }
    else if(document.getElementById("weird").chcecked === true){
      return BrushType.weirdAfter();
    }
    else if(document.getElementById("dotted").checked === true){
      return BrushType.dottedAfter();
    }
    else if(document.getElementById("earser").checked === true){
      return BrushType.earserAfter();
    }
  },
  // Brushes
  // Normal Brush
  normalBefore: function() {
    // Drawing line
    // Defining color of line    
    ctx.strokeStyle = Brush.color;
    ctx.beginPath();
    // Defining size of brush
    ctx.lineWidth = Brush.size;
    // Start point of line
    ctx.moveTo(x, y);
    // Lines ending >> lines will be rounded on ends
    ctx.lineCap = 'round';
  },
  normalAfter: function() {
    // End point of line
    ctx.lineTo(x, y);
    // Draw line
    ctx.stroke();
    cStep++;
  },
  // Weird Brush
  weirdBefore: function() {
    if(Brush.size != 1) {
      ctx.fillStyle = Brush.color;
      ctx.beginPath();
      ctx.arc(x, y, Brush.size / 2, Math.PI * 2, 0, false);
      ctx.closePath();
      ctx.fill();
	}
    
    ctx.strokeStyle = Brush.color;
    ctx.beginPath();
    ctx.lineWidth = Brush.size;
    ctx.moveTo(x, y);
  },
  weirdAfter: function() {
    ctx.lineTo(x, y);
    ctx.stroke();	
    cStep++;
    
	if(Brush.size != 1) {
      ctx.fillStyle = "green";
      ctx.beginPath();
      ctx.arc(x, y, Brush.size / 4, Math.PI * 2, 0, false);
      ctx.closePath();
      ctx.fill();
      ctx.fillStyle = Brush.color;
      ctx.beginPath();
      ctx.arc(x, y, Brush.size / 2, Math.PI * 2, 0, false);
      ctx.closePath();
      ctx.fill();
	}
  },
  // Dotted Brush
  dottedAfter: function() {
    cStep++;
    for(i = 0; i < Brush.number; i++){
      size = Brush.size / 2;
      randomNumber_1 = Math.round(Math.random() * (size + size) - size);
      randomNumber_2 = Math.round(Math.random() * (size + size) - size);
      if(randomNumber_1 == randomNumber_2 && randomNumber_1 > size / 2){
        randomNumber_1 = Math.round(Math.random() * (size / 2 + size / 2) - size / 2);
        randomNumber_2 = Math.round(Math.random() * (size / 2 + size / 2) - size / 2);
      }
      ctx.fillStyle = Brush.color;
      ctx.beginPath();
      ctx.arc(x + randomNumber_1, y + randomNumber_2, 1, Math.PI * 2, 0, false);
      ctx.closePath();
      ctx.fill();
    }
  },
  earserAfter: function()
  {
    ctx.globalCompositeOperation = "destination-out";
    ctx.lineTo(x, y);
    ctx.stroke();
  }
};

// Clearing of canvas
function clear() {
  canvas.width = canvas.width;
}

// Check if mouse is down
function drawTrue() {
  isMouseDown = true;
}

// Check if mouse is down
function drawFalse() {
  first = 1;
  isMouseDown = false;
}

// Function of drawing
function draw(ev) {
  // Check if mouse is down or not
  if(isMouseDown){
    if(first == 1) {
      // Return mouse X position to var x
      x = ev.clientX - canvas.offsetLeft;
      // Return mouse Y position to var y
      y = ev.clientY - canvas.offsetTop;
      first = 0;
    }
    else if(first === 0){
      // Calling of brush function
      BrushType.selectBrushBefore();
      x = ev.clientX - canvas.offsetLeft;
      y = ev.clientY - canvas.offsetTop;
      // Calling of other brush function
      BrushType.selectBrushAfter();
    }
  }
}

var dwn = document.getElementById('btndownload');

dwn.onclick = function(){
  download(canvas, 'image.png');
}

function download(canvas, filename) {
  var link = document.createElement('a');
  link.download = filename;
  link.href = canvas.toDataURL("image/png;base64");
  if (document.createEvent) {
    e = document.createEvent("MouseEvents");
    e.initMouseEvent("click", true, true, window,
                     0, 0, 0, 0, 0, false, false, false,
                     false, 0, null);
    link.dispatchEvent(e);
  } else if (link.fireEvent) {
    link.fireEvent("onclick");
  }
}

var state = context.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

canvas.addEventListener('mouseup',function(){ 
  isMouseDown = false;
  var state = context.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);
});

var redo = document.getElementById('redo');

redo.addEventListener('click', function(){
  cStep++;
  var canvasPic = new Image();
  canvasPic.src = cPushArray[cStep];
  canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
}, false);

var undo = document.getElementById('undo');

undo.addEventListener('click', function(){
  cStep--;
  var canvasPic = new Image();
  canvasPic.src = cPushArray[cStep];
  canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
}, false);
