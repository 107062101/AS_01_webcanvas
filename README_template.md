# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | N         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | N         |
| Un/Re-do button                                  | 10%       | N         |
| Image tool                                       | 5%        | N         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 

    運用左邊的工具列來選擇工具，分別有畫筆（圖片來不及用）、顏色與功能。上方可以調整筆刷大小以及刷毛疏密度。下方可以清空整個畫布。

### Function description

    額外功能是：左邊功能的PEN除了第一個一般的畫筆外，第二格則是像滴墨一樣，用隨機的方式讓畫筆之間有空格。
    第三格是噴漆，像小畫家一樣用隨機的方式在筆刷大小內噴霧。

### Gitlab page link

    https://107062101.gitlab.io/AS_01_webcanvas 

### Others (Optional)

    我覺得這次的作業很棒，內容量頗大且需要時間吸收。
	我很後悔這麼晚才開始做作業，因為我一開始覺得上網查詢資料，稍微了解一下後就可以隨便寫寫結束了。
	結果網路上教學五花八門，而且有些也不是靠google就能夠學到的東西，因此沒有辦法完整這次的作業，我感到很愧疚也很抱歉。

<style>
table th{
    width: 100%;
}
</style>